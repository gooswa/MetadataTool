//
//  main.swift
//  MetadataTool
//
//  Created by Justin Gaussoin on 5/20/16.
//  Copyright © 2016 Justin Gaussoin. All rights reserved.
//

import Foundation
import Cocoa


//let filePath = "/Volumes/Projection/Tests/thebuzz.m4a"
//let filePath = "/Volumes/Projection/Tests/feelinggood.m4a"
//let filePath = "/Volumes/Projection/Tests/timetomove.m4a"
//let filePath = "/Volumes/Projection/Tests/petergunn.m4a"
//let filePath = "/Volumes/Projection/Tests/quicksand.aif"
//let filePath = "/Volumes/Projection/Tests/rocky_lsd.aiff"
//let filePath = "/Volumes/Projection/Tests/ontheway.mp3"
//let filePath = "/Volumes/Projection/Tests/14H54.mp3"
//let filePath = "/Volumes/Projection/Tests/hello.mp3"
//let filePath = "/Volumes/Projection/Tests/quicksand_notag.aif"



var info = mach_timebase_info(numer: 0, denom: 0)
mach_timebase_info(&info)

let baseTime = (info.numer / info.denom)
let startTime = mach_absolute_time()

func main() {
    do {
        let newImageFile = "/Volumes/Projection/Tests/BarkleyElsewhere.jpg"
        //let filePath = "/Volumes/Projection/Tests/ontheway.mp3"
        let filePath = "/Volumes/Projection/IsolatedTest/petergunn.mp3"
        //let filePath = "/Volumes/Projection/IsolatedTest/petergunn.m4a"
        
        guard let coverImage = NSImage(contentsOfFile: newImageFile) else {
            throw NSError(domain: "CoverImageDomain", code: 0, userInfo: nil)
        }
        
        let metadata = try Metadata.parse(filePath)
        metadata.albumArt = coverImage
        metadata.saveToFile()
        
    } catch let err {
        print("Error \(err)")
    }
}
autoreleasepool {
    main()
}


let deltaTime = mach_absolute_time() - startTime
let delta = deltaTime * UInt64(baseTime)

let milliSeconds = Double(delta) / Double(NSEC_PER_MSEC)
print(milliSeconds)




    