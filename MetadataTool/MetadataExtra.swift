//
//  MetadataError.swift
//  MetadataTool
//
//  Created by Justin Gaussoin on 5/20/16.
//  Copyright © 2016 Justin Gaussoin. All rights reserved.
//

import Foundation
#if os(OSX)
  import Cocoa
#endif

private extension String {
    func asciiTagValue() -> [UInt8] {
        var array: [UInt8] = Array(self.utf8)
        if array.count == 5 {
            array.removeFirst()
        }
        return array
    }
    func dataArray() -> [UInt8] {
        return Array(self.utf8)
    }
}



internal extension UnsafePointer {
    func swappedAsInt() -> Int {
        return Int(self.uint32Swapped())
    }
    
    func uint32Swapped() -> UInt32 {
        return UnsafePointer<UInt32>(self)[0].byteSwapped
    }
    func uint8Pointer() -> UnsafePointer<UInt8> {
        return UnsafePointer<UInt8>(self)
    }
    

    
    func uint32Pointer() -> UnsafePointer<UInt32> {
        return UnsafePointer<UInt32>(self)
    }
    
    func syncSafe24BitInt() -> Int {
        let uint8self = uint8Pointer()
        var size = 0
        var shift = 21
        for i in 0..<4 {
            size += Int(uint8self[i]) << shift
            shift -= 7
        }
        return Int(size)
    }
}

extension Int {
    func syncSafe24Bit() -> UInt32 {
        var newSize:UInt32 = 0
        
        for i in 0 ..< 4 {
            // Get the bytes from size
            let shift = i * 8
            let mask = 0xFF << shift

            // Shift the byte down in order to use the mask
            var byte = (self & mask) >> shift
            
            var oMask: UInt8 = 0x80
            for _ in 0 ..< i {
                // Create the overflow mask
                oMask = oMask >> 1
                oMask += 0x80
            }
            
            // The left side of the byte
            let overflow = UInt8(byte) & oMask
            
            // The right side of the byte
            let untouched = UInt8(byte) & ~oMask
            
            // Store the byte
            byte = ((Int(overflow) << 1) + Int(untouched)) << (shift + i)
            newSize += UInt32(byte)
        }
        return newSize.byteSwapped
    }
}

public enum MetadataFileType {
    case m4a
    case mp3
    case aif
    
    var artist:String {
        switch self {
        case m4a: return "©ART"
        case mp3, aif: return "TPE1"
        }
    }
    
    var songName:String {
        switch self {
        case m4a: return "©nam"
        case mp3, aif: return "TIT2"
        }
    }
    
    var albumArt:String {
        switch self {
        case m4a: return "covr"
        case mp3, aif: return "APIC"
        }
    }
    
    var album:String {
        switch self {
        case m4a: return "©alb"
        case mp3, aif: return "TALB"
        }
    }
    
    var albumArtist:String {
        switch self {
        case m4a: return "aART"
        case mp3, aif: return "TPE2"
        }
    }
    
    var genre:String {
        switch self {
        case m4a: return "©gen"
        case mp3, aif: return "TCON"
        }
    }
    
    var date:String {
        switch self {
        case m4a: return "©day"
        case mp3, aif: return "TDRC"
        }
    }
    
    var track:String {
        switch self {
        case m4a: return "trkn"
        case mp3, aif: return "TRCK"
        }
    }
    
    var disc:String {
        switch self {
        case m4a: return "disk"
        case mp3, aif: return "TPOS"
        }
    }
    
    var compilation:String {
        switch self {
        case m4a: return "cpil"
        case mp3, aif: return "TCMP"
        }
    }
    
//    var rating:String {
//        switch self {
//        case m4a: return "rtng"
//        case mp3, aif: return "xxxx"
//        }
//    }
    
    var bpm:String {
        switch self {
        case m4a: return "tmpo"
        case mp3, aif: return "TBPM"
        }
    }
    
    var comment:String {
        switch self {
        case m4a: return "©cmt"
        case mp3, aif: return "COMM"
        }
    }
    
    
    var key:String {
        switch self {
        case m4a: return "initialkey"
        case mp3, aif: return "TKEY"
        }
    }
    
    var energyLevel:String {
        switch self {
        case m4a: return "energylevel"
        case mp3, aif: return "TIT1"
        }
    }
}

public enum MetadataType {
    case string
    case uint8
    case uint16
    case uint32
    case uint64
    case custom
    case jpg
    case png
    case sequence
    case unknown
}

public struct MetadataError : ErrorType {
    public enum ErrorType {
        case noFile
        case unsupportedFile
        case noTagFound
        case noParser
    }
    
    var type:ErrorType
    var description:String?
    
    public init(type t:ErrorType, description desc:String?=nil) {
        type = t
        description = desc
    }
}

public typealias MetaSequence = (index:Int?, total:Int?)




public class MetadataItem {
    public var tag:String = "xxxx"
    public var type:MetadataType = .unknown
    public var value:NSData?
    
    var stringValue:String? {
        if let val = value {
            return String(data: val, encoding: NSUTF8StringEncoding)
        }
        return nil
    }
    
    var boolValue:Bool? {
        if let val = value {
            return Bool(Int(UnsafePointer<UInt8>(val.bytes)[0]))
        }
        return nil
    }

    var int8Value:UInt8? {
        if let val = value {
            return UnsafePointer<UInt8>(val.bytes)[0]
        }
        return nil
    }
    
    var int16Value:UInt16? {
        if let val = value {
            return UnsafePointer<UInt16>(val.bytes)[0].bigEndian
        }
        return nil
    }
    
    var int32Value:UInt32? {
        if let val = value {
            return UnsafePointer<UInt32>(val.bytes)[0].bigEndian
        }
        return nil
    }
    
    var int64Value:UInt64? {
        if let val = value {
            return UnsafePointer<UInt64>(val.bytes)[0].bigEndian
        }
        return nil
    }
    
    #if os(OSX)
    var imageData:NSImage? {
        if let val = value {
            return NSImage(data: val)
        }
        return nil
    }
    #endif
    
    init(tag _tag:String, data:NSData?=nil) {
        tag = _tag
        if let d = data {
            parse(d)
        }
    }
    
    func parse(data:NSData) {
        
    }
    
    public func encode() -> NSData? {
        return nil
    }
}

public class AtomMetadataItem : MetadataItem {
    
    override func parse(data:NSData) {
        
        var pointer = data.bytes.uint8Pointer()
        pointer += 8
        
        let dataSize = pointer.swappedAsInt()
        let payloadPointer = pointer + 16
        let payloadType = (pointer + 8)[3]
        let payloadSize = dataSize - 16
        
        switch payloadType {
        case 1:
            type = .string
        case 21:
            switch payloadSize {
            case 1: type = .uint8
            case 2: type = .uint16
            case 4: type = .uint32
            case 8: type = .uint64
            default: ()
            }
        case 13:
            type = .jpg
        case 14:
            type = .png
        default: ()
        }
        value = NSData(bytes: payloadPointer, length: payloadSize)
    }
    
    
    override public func encode() -> NSData? {
        guard let val = value else {
            return nil
        }
        let nullBytes:[UInt8] = [0x00, 0x00, 0x00, 0x00]
        
        let mutData = NSMutableData()
        var dataSize:UInt32 = (16 + UInt32(val.length)).byteSwapped
        var size:UInt32 = (24 + UInt32(val.length)).byteSwapped
        
        mutData.appendBytes(&size, length: 4)
        mutData.appendBytes(tag.asciiTagValue(), length: 4)
        
        mutData.appendBytes(&dataSize, length: 4)
        mutData.appendBytes("data", length: 4)
        var typeByte:UInt8 = 0
        switch type {
        case .string:
            typeByte = 1
        case .uint8, .uint16, .uint32, .uint64:
            typeByte = 21
        case .jpg:
            typeByte = 13
        case .png:
            typeByte = 14
        default: ()
        }
        let flags:[UInt8] = [0x00, 0x00, 0x00, typeByte]
        mutData.appendBytes(flags, length: 4)
        mutData.appendBytes(nullBytes, length: 4)
        mutData.appendData(val)
        
        return mutData
    }
}

public class AtomSequenceItem : AtomMetadataItem {
    public var index:UInt16?
    public var total:UInt16?
    
    override init(tag _tag: String, data: NSData?=nil) {
        super.init(tag: _tag, data: data)
        
        guard let val = value else {
            return
        }
        type = .sequence
        let pointer = val.bytes.uint8Pointer()
        index = UnsafePointer<UInt16>(pointer+2)[0].bigEndian
        if UnsafePointer<UInt16>(pointer+4)[0].bigEndian != 0 {
            total = UnsafePointer<UInt16>(pointer+4)[0].bigEndian
        }
        
    }
}

public class AtomExtendedItem : MetadataItem {
    public var mean:String?
    public var name:String?

    override func parse(data:NSData) {
        
        var pointer = data.bytes.uint8Pointer()
        pointer += 8
        
        let meanSize = pointer.swappedAsInt()
        mean = String(data: NSData(bytes: pointer + 12, length: meanSize - 12), encoding: NSUTF8StringEncoding)
        
        pointer += meanSize
        
        let nameSize = pointer.swappedAsInt()
        name = String(data: NSData(bytes: pointer + 12, length: nameSize - 12), encoding: NSUTF8StringEncoding)

        pointer += nameSize
        
        let dataSize = pointer.swappedAsInt()
        let payloadPointer = pointer + 16
        let payloadType = (pointer + 8)[3]
        let payloadSize = dataSize - 16
        
        type = payloadType == 1 ? .string : .custom
        value = NSData(bytes: payloadPointer, length: payloadSize)
    }
    
    override public func encode() -> NSData? {
        guard let val = value else {
            return nil
        }
        let nullBytes:[UInt8] = [0x00, 0x00, 0x00, 0x00]
        
        let mutData = NSMutableData()
        mutData.appendBytes(nullBytes, length: 4)
        mutData.appendBytes(tag.asciiTagValue(), length: 4)
        
        let m = mean!.dataArray()
        var meanSize = (12 + UInt32(m.count)).byteSwapped
        mutData.appendBytes(&meanSize, length: 4)
        mutData.appendBytes("mean", length: 4)
        mutData.appendBytes(nullBytes, length: 4)
        mutData.appendBytes(m, length: m.count)
        
        let n = name!.dataArray()
        var nameSize = (12 + UInt32(n.count)).byteSwapped
        mutData.appendBytes(&nameSize, length: 4)
        mutData.appendBytes("name", length: 4)
        mutData.appendBytes(nullBytes, length: 4)
        mutData.appendBytes(n, length: n.count)
        
        var dataSize:UInt32 = (16 + UInt32(val.length)).byteSwapped
        mutData.appendBytes(&dataSize, length: 4)
        mutData.appendBytes("data", length: 4)
        var typeByte:UInt8 = 0
        switch type {
        case .string:
            typeByte = 1
        case .uint8, .uint16, .uint32, .uint64:
            typeByte = 21
        case .jpg:
            typeByte = 13
        case .png:
            typeByte = 14
        default: ()
        }
        let flags:[UInt8] = [0x00, 0x00, 0x00, typeByte]
        mutData.appendBytes(flags, length: 4)
        mutData.appendBytes(nullBytes, length: 4)
        mutData.appendData(val)
        
        var realSize:UInt32 = UInt32(mutData.length).byteSwapped
        mutData.replaceBytesInRange(NSMakeRange(0, 4), withBytes: &realSize)
        
        return mutData
    }
}

internal enum ID3TextEncoding : UInt8 {
    case iso = 0x00
    case utf16 = 0x01
    case utf16be = 0x02
    case utf8 = 0x03
    
    internal var textEncoding:UInt {
        switch self {
        case .iso:
            return NSISOLatin1StringEncoding
        case .utf16:
            return NSUTF16StringEncoding
        case .utf16be:
            return NSUTF16BigEndianStringEncoding
        case .utf8:
            return NSUTF8StringEncoding
        }
    }
}

public class ID3MetadataItem : MetadataItem {
    private var flags:[UInt8] = [0x00, 0x00]
    internal var encoding:ID3TextEncoding!
    
    
    override var stringValue:String? {
        if let val = value {
            return String(data: val, encoding: encoding.textEncoding)
        }
        return nil
    }
    
    override func parse(data:NSData) {
        var pointer = data.bytes.uint8Pointer()
        pointer += 4 // Skip Tag
        
        let size = pointer.syncSafe24BitInt() - 1
        pointer += 4 //Moving to flags
        
        flags[0] = pointer[0]
        flags[1] = pointer[1]
        
        pointer += 2 //Moving to text encoding
        encoding = ID3TextEncoding(rawValue: pointer[0])!
        pointer += 1
        
        type = .string
        value = NSData(bytes: pointer, length: size)
    }
    
    override public func encode() -> NSData? {
        guard let val = value else {
            return nil
        }
        let data = NSMutableData()
        
        let tagData = tag.dataArray()
        data.appendBytes(tagData, length: 4)
        
        var syncSize = (val.length + 1).syncSafe24Bit()
        data.appendBytes(&syncSize, length: 4)
        
        data.appendBytes(&flags, length: 2)
        var rawEncoding = encoding.rawValue
        data.appendBytes(&rawEncoding, length: 1)

        data.appendData(val)
        
        return data
    }
    
}

public class ID3TextExtraItem : ID3MetadataItem {
    internal var key:String = "GenericKey"
    override func parse(data:NSData) {
        var pointer = data.bytes.uint8Pointer()
        pointer += 4 // Skip Tag
        
        let size = pointer.syncSafe24BitInt() - 1
        pointer += 4 //Moving to flags
        
        flags[0] = pointer[0]
        flags[1] = pointer[1]
        
        pointer += 2 //Moving to text encoding
        encoding = ID3TextEncoding(rawValue: pointer[0])!
        pointer += 1
        
        type = .string
        
        var keyBytes:[UInt8] = []
        while pointer.memory != 0 {
            keyBytes.append(pointer[0])
            pointer += 1
        }
        key = String(data: NSData(bytes: keyBytes, length: keyBytes.count), encoding: NSUTF8StringEncoding) ?? "GenericKey"
        let actualValueSize = size - (keyBytes.count+1)
        value = NSData(bytes: pointer+1, length: actualValueSize)
    }
    
    
    override public func encode() -> NSData? {
        guard let val = value else {
            return nil
        }
        let data = NSMutableData()
        
        let tagData = tag.dataArray()
        data.appendBytes(tagData, length: 4)
        
        var syncSize = (val.length + 1 + key.characters.count + 1).syncSafe24Bit()
        data.appendBytes(&syncSize, length: 4)
        
        data.appendBytes(&flags, length: 2)
        var rawEncoding = encoding.rawValue
        data.appendBytes(&rawEncoding, length: 1)
        data.appendBytes(key, length: key.characters.count)
        var nullByte:UInt8 = 0x00
        data.appendBytes(&nullByte, length: 1)
        data.appendData(val)
        
        return data
    }
}

public class ID3OtherItem : MetadataItem {
    
    override func parse(data:NSData) {
        var pointer = data.bytes.uint8Pointer()
        pointer += 4 // Skip Tag
        
        let size = pointer.syncSafe24BitInt()
        pointer += 4 //Move over size
        value = NSData(bytes: pointer, length: size + 2)
    }
    
    override public func encode() -> NSData? {
        guard let val = value else {
            return nil
        }
        let data = NSMutableData()
        
        let tagData = tag.dataArray()
        data.appendBytes(tagData, length: 4)
        
        var syncSize = (val.length - 2).syncSafe24Bit()
        data.appendBytes(&syncSize, length: 4)
        
        data.appendData(val)
        
        return data
    }
}

public class ID3SequenceItem : ID3MetadataItem {
    public var index:UInt16?
    public var total:UInt16?
    
    
    override init(tag _tag: String, data: NSData?=nil) {
        super.init(tag: _tag, data: data)
        
        guard let strValue = stringValue else {
            return
        }
        type = .sequence
        
        let components = strValue.componentsSeparatedByString("/")
        index = UInt16(components[0])
        total = UInt16(components[1])
        
    }
    
    
    override public func encode() -> NSData? {
        
        let realIndex = index ?? 1
        let realTotal = total ?? 1
        
        guard let strData = "\(realIndex)/\(realTotal)".dataUsingEncoding(NSUTF8StringEncoding) else {
            return nil
        }
        
        let data = NSMutableData()
        let tagData = tag.dataArray()
        data.appendBytes(tagData, length: 4)
        
        var syncSize = (strData.length + 1).syncSafe24Bit()
        
        data.appendBytes(&syncSize, length: 4)
        
        data.appendBytes(&flags, length: 2)
        var rawEncoding = ID3TextEncoding.utf8.rawValue
        data.appendBytes(&rawEncoding, length: 1)
        
        data.appendData(strData)
        
        return data
    }
}


public class ID3CommentItem : MetadataItem {
    private var flags:[UInt8] = [0x00, 0x00]
    internal var encoding:ID3TextEncoding = .iso
    private var language:String = "eng"
    
    override func parse(data:NSData) {
        var pointer = data.bytes.uint8Pointer()
        pointer += 4 // Skip Tag
        
        let size = pointer.syncSafe24BitInt() - 5
        pointer += 4 //Moving to flags
        
        flags[0] = pointer[0]
        flags[1] = pointer[1]
        
        pointer += 2 //Moving to text encoding
        encoding = ID3TextEncoding(rawValue: pointer[0])!
        pointer += 1
        
        language = String(data: NSData(bytes: pointer, length: 3), encoding: NSUTF8StringEncoding) ?? "eng"
        
        pointer += 4 //Move over language and null byte
        
        type = .string
        value = NSData(bytes: pointer, length: size)
    }
    
    override public func encode() -> NSData? {
        guard let val = value else {
            return nil
        }
        let data = NSMutableData()
        
        let tagData = tag.dataArray()
        data.appendBytes(tagData, length: 4)
        
        var syncSize = (val.length + 5).syncSafe24Bit()
        data.appendBytes(&syncSize, length: 4)
        
        data.appendBytes(&flags, length: 2)
        var rawEncoding = encoding.rawValue
        data.appendBytes(&rawEncoding, length: 1)
        data.appendData(language.dataUsingEncoding(NSUTF8StringEncoding)!)
        var nilByte:UInt8 = 0x00
        data.appendBytes(&nilByte, length: 1)
        data.appendData(val)
        
        return data
    }
}

internal enum ID3PictureType : UInt8 {
    case other = 0x00
    case fileIcon = 0x01
    case otherIcon = 0x02
    case coverFront = 0x03
    case coverBack = 0x04
    case leaflet = 0x05
}

public class ID3AlbumArtItem : MetadataItem {
    private var flags:[UInt8] = [0x00, 0x00]
    internal var encoding:ID3TextEncoding!
    internal var mimeType:String = "image/"
    internal var pictureType:ID3PictureType = .other
    
    override func parse(data:NSData) {
        var pointer = data.bytes.uint8Pointer()
        pointer += 4 // Skip Tag
        
        let size = pointer.syncSafe24BitInt()
        
        pointer += 4 //shifting over size to flags
        
        flags[0] = pointer[0]
        flags[1] = pointer[1]
        
        pointer += 2 //shifting over flags to text encoding
        encoding = ID3TextEncoding(rawValue: pointer[0])!
        pointer += 1
        
        var mimeTypeBytes:[UInt8] = []
        while pointer.memory != 0 {
            mimeTypeBytes.append(pointer[0])
            pointer += 1
        }
        let mimeTextData = NSData(bytes: mimeTypeBytes, length: mimeTypeBytes.count)
        mimeType = String(data: mimeTextData, encoding: NSUTF8StringEncoding) ?? "image/"
        pointer += 1 //advanced to picture type
        
        pictureType = ID3PictureType(rawValue: pointer.memory) ?? .other
        pointer += 2 //Skip over null
        
        let offsetSize = mimeTypeBytes.count + 4
        let picDataSize = size - offsetSize
        value = NSData(bytes: pointer, length: picDataSize)
        
    }
    
    override public func encode() -> NSData? {
        guard let val = value else {
            return nil
        }
        let data = NSMutableData()
        
        var nullByte:UInt8 = 0x00

        let calcSize = val.length + mimeType.characters.count + 4
        
        let tagData = tag.dataArray()
        data.appendBytes(tagData, length: 4)
        
        var syncSize = calcSize.syncSafe24Bit()
        data.appendBytes(&syncSize, length: 4)
        
        data.appendBytes(&flags, length: 2)
        var rawEncoding = encoding.rawValue
        data.appendBytes(&rawEncoding, length: 1)
        data.appendData(mimeType.dataUsingEncoding(NSUTF8StringEncoding)!)
        data.appendBytes(&nullByte, length: 1)
        var rawPictureType = pictureType.rawValue
        data.appendBytes(&rawPictureType, length: 1)
        data.appendBytes(&nullByte, length: 1)
        data.appendData(val)
        return data
    }
}






