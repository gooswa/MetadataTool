//
//  MetadataTool.swift
//  MetadataTool
//
//  Created by Justin Gaussoin on 5/20/16.
//  Copyright © 2016 Justin Gaussoin. All rights reserved.
//

import Foundation
#if os(OSX)
    import Cocoa
    
    public extension NSImage {
        var PNGData:NSData {
            
            return NSBitmapImageRep(data: TIFFRepresentation!)!.representationUsingType(.NSPNGFileType, properties: [NSImageCompressionFactor: 0.8])!
        }
        var JPGData:NSData {
            let imageRep = NSBitmapImageRep(data: self.TIFFRepresentation!)
            return imageRep!.representationUsingType(.NSJPEGFileType, properties: [NSImageCompressionFactor: 0.8])!
        }
    }
    
#elseif os(iOS)
    import UIKit
#endif



private struct TagFinder {
    private static let searchChunkSize:Int = 1024000 //512000 * 2
    
    private static func findTag(inFile:NSFileHandle, type:MetadataFileType) throws -> Int {
        
        var tagString = "ID3"
        
        switch type {
        case .m4a:
            tagString = "meta"
        case .aif, .mp3:
            tagString = "ID3"
        }
        
        guard let tag = tagString.dataUsingEncoding(NSUTF8StringEncoding) else { //tagString.data(using: NSUTF8StringEncoding)
            throw MetadataError(type: .noTagFound, description: "Can't find tag in file \(inFile) for specficied type.")
        }
        
        switch type {
        case .m4a, .mp3:
            return try TagFinder._findTag(inFile: inFile, tag: tag)
        case .aif:
            return try TagFinder._findTagReverse(inFile: inFile, tag: tag)
        }
    }
    
    private static func _findTag(inFile file:NSFileHandle, tag:NSData) throws -> Int {
        var tagIndex = -1
        let fileLength = file.seekToEndOfFile()
        file.seekToFileOffset(0) //file.seek(toFileOffset: 0)
        repeat {
            var amountToRead:Int = TagFinder.searchChunkSize
            
            amountToRead = ( file.offsetInFile + UInt64(amountToRead) ) >= fileLength ? Int( fileLength - file.offsetInFile ) : amountToRead
            
            let chunk = file.readDataOfLength( amountToRead ) //file.readData(ofLength: amountToRead)
            let range = chunk.rangeOfData(tag, options: [], range: NSMakeRange(0, chunk.length)) //chunk.range(of: tag, options: [], in: NSMakeRange(0, chunk.length))
            
            if range.location != Int.max {
                tagIndex = range.location
                break
            }
            
            file.seekToFileOffset(file.offsetInFile - UInt64(tag.length))//file.seek(toFileOffset: file.offsetInFile - UInt64(tag.length))
            
        } while file.offsetInFile < fileLength
        if tagIndex == -1 {
            throw MetadataError(type: .noTagFound, description: "Can't find tag in file \(file) for specficied type.")
        }
        return tagIndex
    }
    
    private static func _findTagReverseFromPosition(inFile file:NSFileHandle, tag:NSData, position:UInt64) throws -> Int {
        file.seekToFileOffset(position - 128)
        var tagIndex = -1
        var currentOffset:UInt64 = 0
        var isLastSearch:Bool = false
        repeat {
            currentOffset = file.offsetInFile
            let amountToRead:Int = 128
            let chunk = file.readDataOfLength( amountToRead )//file.readData(ofLength: amountToRead)
            let range = chunk.rangeOfData(tag, options: [.Backwards], range: NSMakeRange(0, chunk.length))//chunk.range(of: tag, options: [.backwards], in: NSMakeRange(0, chunk.length))
            if range.location != Int.max {
                tagIndex = Int(currentOffset) + range.location
                break
            }
            
            if isLastSearch {
                break
            }
            
            isLastSearch = (Int(currentOffset) - TagFinder.searchChunkSize + tag.length) < 0
            let newOffset = isLastSearch ? 0 : Int(currentOffset) - TagFinder.searchChunkSize + tag.length
            file.seekToFileOffset(UInt64(newOffset))//file.seek(toFileOffset: UInt64(newOffset))
        } while file.offsetInFile >= 0
        return tagIndex
    }
    
    private static func _findTagReverse(inFile file:NSFileHandle, tag:NSData) throws -> Int {
        var tagIndex = -1
        let fileLength = file.seekToEndOfFile()
        file.seekToFileOffset(fileLength - UInt64(TagFinder.searchChunkSize))//file.seek(toFileOffset: fileLength - UInt64(Metadata.tagSearchChunkSize))
        var currentOffset:UInt64 = 0
        var isLastSearch:Bool = false
        repeat {
            currentOffset = file.offsetInFile
            let amountToRead:Int = TagFinder.searchChunkSize
            let chunk = file.readDataOfLength( amountToRead )//file.readData(ofLength: amountToRead)
            let range = chunk.rangeOfData(tag, options: [.Backwards], range: NSMakeRange(0, chunk.length))//chunk.range(of: tag, options: [.backwards], in: NSMakeRange(0, chunk.length))
            if range.location != Int.max {
                tagIndex = Int(currentOffset) + range.location
                break
            }
            
            if isLastSearch {
                break
            }
            
            isLastSearch = (Int(currentOffset) - TagFinder.searchChunkSize + tag.length) < 0
            let newOffset = isLastSearch ? 0 : Int(currentOffset) - TagFinder.searchChunkSize + tag.length
            file.seekToFileOffset(UInt64(newOffset))//file.seek(toFileOffset: UInt64(newOffset))
        } while file.offsetInFile >= 0
        return tagIndex
    }
}



public class Metadata {
    private static let filetypeExtensions:[String:MetadataFileType] = [
        "aif" : .aif,
        "m4a" : .m4a,
        "mp3" : .mp3,
        "aiff" : .aif
    ]
    
    private var file:NSFileHandle
    public private(set) var type:MetadataFileType
    private var _tagIndex:Int = -1
    private var originalMetaSize:Int = 0

    public var artist: String? {
        set (value) {
            _setValue(type.artist, value: value)
        }
        get {
            return items[type.artist]?.stringValue
        }
    }
    
    public var songName: String? {
        set (value) {
            _setValue(type.songName, value: value)
        }
        get {
            return items[type.songName]?.stringValue
        }
    }
    
    #if os(OSX)
    public var albumArt:NSImage?
    #elseif os(iOS)
    public var albumArt:UIImage?
    #endif
    
    
    public var album: String? {
        set (value) {
            
            _setValue(type.album, value: value)
        }
        get {
            return items[type.album]?.stringValue
        }
    }
    
    public var albumArtist: String? {
        set (value) {
            _setValue(type.albumArtist, value: value)
        }
        get {
            return items[type.albumArtist]?.stringValue
        }
    }
    
    
    public var genre: String? {
        set (value) {
            _setValue(type.genre, value: value)
        }
        get {
            return items[type.genre]?.stringValue
        }
    }
    
    public var date: String? {
        set (value) {
            _setValue(type.date, value: value)
        }
        get {
            return items[type.date]?.stringValue
        }
    }
    
    public var track:MetaSequence?
    public var disc:MetaSequence?
    
    
    public var compilation:Bool? {
        set (value) {
            _setValue(type.compilation, value: value)
        }
        get {
            return items[type.compilation]?.boolValue
        }
    }
    
//    public var rating:UInt8? {
//        set (value) {
//            _setValue(type.rating, value: value)
//        }
//        get {
//            return items[type.rating]?.int8Value
//        }
//    }

    
    public var bpm:UInt16?  {
        set (value) {
            _setValue(type.bpm, value: value)
        }
        get {
            return items[type.bpm]?.int16Value
        }
    }
    
    public var comment:String? {
        set (value) {
            _setValue(type.comment, value: value)
        }
        get {
            return items[type.comment]?.stringValue
        }
    }
    
    public var key:String? {
        set (value) {
            _setValue(type.key, value: value)
        }
        get {
            return items[type.key]?.stringValue
        }
    }
    
    
    public var energyLevel:String?  {
        set (value) {
            _setValue(type.energyLevel, value: value)
        }
        get {
            return items[type.energyLevel]?.stringValue
        }
    }
    
    
    public var items:[String:MetadataItem] = [:]
    private var _keyOrder:[String] = []
    
    private init(fileHandle:NSFileHandle, type t:MetadataFileType) throws {
        file = fileHandle
        type = t
        _tagIndex = try TagFinder.findTag(file, type: type)
        let data = try _getMetadataFromFile(_tagIndex)
        originalMetaSize = data.length
        try _parse(data)
    }
    
    public class func parse(filePath:String) throws -> Metadata {
        do {
            return try Metadata.parse(NSURL(fileURLWithPath: filePath))
        } catch let err {
            throw err
        }
    }
    
    public class func parse(url:NSURL) throws -> Metadata {
        let filePath = url.path!
        do {
            let file = try NSFileHandle(forUpdatingURL: url)
            
            if let fileSplit = url.lastPathComponent?.componentsSeparatedByString(".") {
                let fileExtension = fileSplit[fileSplit.count - 1]
                if let fileType = Metadata.filetypeExtensions[fileExtension] {
                    
                    switch fileType {
                    case .aif, .mp3:
                        let meta = try Mp3Metadata(fileHandle: file, type: fileType)
                        return meta
                    case .m4a:
                        let meta = try AtomMetadata(fileHandle: file, type: fileType)
                        return meta
                    }
                    
                } else {
                    throw MetadataError(type: .unsupportedFile, description: "No file extension for file.")
                }
            }
        } catch let err {
            if err._code == 2 && err._domain == "NSCocoaErrorDomain" {
                throw MetadataError(type: .noFile, description: "No File could be found at location. \(filePath)")
            }
            throw err
        }
        throw MetadataError(type: .unsupportedFile, description: "No file extension for file.")
    }
    
    private func _getMetadataFromFile(tagIndex:Int) throws -> NSData {
        throw MetadataError(type: .noTagFound, description: "No tag found due to unimplemented _getMetaDataFromFile: \(type)")
    }
    
    private func _parse(data:NSData) throws {
        throw MetadataError(type: .noParser, description: "There is no valid parser for metadata type: \(type)")
    }
    

    private func _setValue<Type>(key:String, value:Type?) {
        if let item = items[key] {
            if value is String {
                item.value = (value as! String).dataUsingEncoding(NSUTF8StringEncoding)
            } else if value is Bool {
                var b:UInt8 = value != nil && (value as! Bool) ? 1 : 0
                item.value = NSData(bytes: &b, length: 1)
            } else if value is UInt8 {
                var v = value as! UInt8
                item.value = NSData(bytes: &v, length: 1)
            } else if value is UInt16 {
                var v = value as! UInt16
                item.value = NSData(bytes: &v, length: 2)
            }

        } else {
            let item = type == .mp3 ? ID3MetadataItem(tag: key) : AtomMetadataItem(tag: key)
            if value is String {
                if let mp3item = item as? ID3MetadataItem {
                    mp3item.encoding = .utf8
                }
                item.value = (value as! String).dataUsingEncoding(NSUTF8StringEncoding)
            } else if value is Bool {
                var b:UInt8 = value != nil && (value as! Bool) ? 1 : 0
                item.value = NSData(bytes: &b, length: 1)
            } else if value is UInt8 {
                var v = value as! UInt8
                item.value = NSData(bytes: &v, length: 1)
            } else if value is UInt16 {
                var v = value as! UInt16
                item.value = NSData(bytes: &v, length: 2)
            }
            items[key] = item
        }
    }
    
    public func saveToFile() {
        
    }
    
    
}


public class AtomMetadata : Metadata {
    
    private var _headerData:NSData?
    private var _hasFreeAtom:Bool = false
    private var _freeSize:Int = 0
    private var _moovTagIndex:Int = 0
    private var _moovTagSize:UInt32 = 0
    private var _udtaTagIndex:Int = 0
    private var _udtaTagSize:UInt32 = 0
    
    
    
    override public var artist: String? {
        set (value) {
            super.artist = value
            if let item = items["soar"] {
                item.value = value?.dataUsingEncoding(NSUTF8StringEncoding)
            }
        }
        get {
            return super.artist
        }
    }
    
    override public var songName: String? {
        set (value) {
            super.songName = value
            if let item = items["sonm"] {
                item.value = value?.dataUsingEncoding(NSUTF8StringEncoding)
            }
        }
        get {
            return super.songName
        }
    }
    
    #if os(OSX)
    override public var albumArt: NSImage? {
        set (value) {
            if let item = items[type.albumArt] {
                item.type = .jpg
                item.value = value?.JPGData
            } else {
                let item = AtomMetadataItem(tag: type.albumArt)
                item.type = .jpg
                item.value = value?.JPGData
                items[type.albumArt] = item
            }
        }
        get {
            return items[type.albumArt]?.imageData
        }
    }
    #endif
    
    override public var album: String? {
        set (value) {
            super.album = value
            if let item = items["soal"] {
                item.value = value?.dataUsingEncoding(NSUTF8StringEncoding)
            }
        }
        get {
            return super.album
        }
    }
    
    override public var track: MetaSequence? {
        set (value) {
            if let v = value {
                var index:UInt16 = v.index != nil ? UInt16(v.index!) : 0
                var total:UInt16 = v.total != nil ? UInt16(v.total!) : 0
                let mutData = NSMutableData()
                mutData.appendBytes(&index, length: 2)
                mutData.appendBytes(&total, length: 2)
                
                if let item = items[type.track] {
                    item.value = mutData
                    
                } else {
                    let item = AtomMetadataItem(tag: type.track)
                    item.value = mutData
                    items[type.track] = item
                }
            }
            
        }
        get {
            if let item = items[type.track] as? AtomSequenceItem {
                let index:Int? = item.index != nil ? Int(item.index!) : nil
                let total:Int? = item.total != nil ? Int(item.total!) : nil
                return MetaSequence(index: index, total: total)
            }
            return nil
        }
    }
    
    override public var disc: MetaSequence? {
        set (value) {
            if let v = value {
                var index:UInt16 = v.index != nil ? UInt16(v.index!) : 0
                var total:UInt16 = v.total != nil ? UInt16(v.total!) : 0
                let mutData = NSMutableData()
                mutData.appendBytes(&index, length: 2)
                mutData.appendBytes(&total, length: 2)
                
                if let item = items[type.disc] {
                    item.value = mutData
                    
                } else {
                    let item = AtomMetadataItem(tag: type.disc)
                    item.value = mutData
                    items[type.track] = item
                }
            }
            
        }
        get {
            if let item = items[type.disc] as? AtomSequenceItem {
                let index:Int? = item.index != nil ? Int(item.index!) : nil
                let total:Int? = item.total != nil ? Int(item.total!) : nil
                return MetaSequence(index: index, total: total)
            }
            return nil
        }
    }
    
    override public var key:String? {
        set (value) {
            if let item = items[type.key] {
                item.value = value?.dataUsingEncoding(NSUTF8StringEncoding)
            } else {
                let item = AtomExtendedItem(tag: "----")
                item.name = "initialkey"
                item.mean = "com.apple.iTunes"
                items[type.key] = item
            }
        }
        get {
            return items[type.key]?.stringValue
        }
    }
    
    
    override public var energyLevel:String?  {
        set (value) {
            if let item = items[type.energyLevel] {
                item.value = value?.dataUsingEncoding(NSUTF8StringEncoding)
            } else {
                let item = AtomExtendedItem(tag: "----")
                item.name = "energylevel"
                item.mean = "com.apple.iTunes"
                items[type.energyLevel] = item
            }
        }
        get {
            return items[type.key]?.stringValue
        }
    }
    
    override init(fileHandle: NSFileHandle, type t: MetadataFileType) throws {
        try super.init(fileHandle: fileHandle, type: t)
        let moovTag = "moov".dataUsingEncoding(NSUTF8StringEncoding)!
        let udtaTag = "udta".dataUsingEncoding(NSUTF8StringEncoding)!
        _moovTagIndex = try TagFinder._findTag(inFile: file, tag: moovTag) - 4
        
        file.seekToFileOffset(UInt64(_moovTagIndex))
        _moovTagSize = UnsafePointer<UInt32>(file.readDataOfLength(4).bytes)[0].byteSwapped
        _udtaTagIndex = try TagFinder._findTagReverseFromPosition(inFile: file, tag: udtaTag, position: UInt64(_tagIndex)) - 4

        file.seekToFileOffset(UInt64(_udtaTagIndex))
        _udtaTagSize = UnsafePointer<UInt32>(file.readDataOfLength(4).bytes)[0].byteSwapped
    }
    
    override private func _getMetadataFromFile(tagIndex:Int) -> NSData {
        let metadataOffset = UInt64(tagIndex-4)
        file.seekToFileOffset(metadataOffset)
        
        let dataBytes = file.readDataOfLength(4).bytes
        let size = dataBytes.uint32Swapped()
        file.seekToFileOffset(metadataOffset)
        return file.readDataOfLength(Int(size))
    }
    
    override private func _parse(data:NSData) throws {
        var pointer = data.bytes.uint8Pointer()
        pointer += 12 //Skip over 'meta' atom

        let headerSize = pointer.swappedAsInt()
        _headerData = NSData(bytes: pointer, length: headerSize) //Store Header Raw Data for Placing back when writing
        
        pointer += headerSize //Get Header Size and Skip over it
        
        let listSize = pointer.swappedAsInt() //Getting size of 'ilist' atom and containting metadata items
        

        let loopSize = (listSize - 8)
        pointer += 8 //Skip to actual metadata items in 'ilst'
        
        var offset = 0
        
        while offset < loopSize {
            let itemPointer = pointer.uint32Pointer()
            let itemSize = itemPointer.swappedAsInt()
            let itemTag = String(data: NSData(bytes: itemPointer + 1, length: 4), encoding: NSASCIIStringEncoding) ?? "void"
            
            if itemTag == "----" {
                
                let item = AtomExtendedItem(tag: itemTag, data: NSData(bytes: pointer, length: itemSize))
                items[item.name!] = item
                _keyOrder.append(item.name!)
            } else if itemTag == "disk" || itemTag == "trkn" {
                let item = AtomSequenceItem(tag: itemTag, data: NSData(bytes: pointer, length: itemSize))
                items[item.tag] = item
                _keyOrder.append(item.tag)
            } else {
                let item = AtomMetadataItem(tag: itemTag, data: NSData(bytes: pointer, length: itemSize))
                items[item.tag] = item
                _keyOrder.append(item.tag)
            }
            
            offset += itemSize
            pointer += itemSize
        }
        if let freeTag = String(data: NSData(bytes: pointer + 4, length: 4), encoding: NSUTF8StringEncoding) where freeTag == "free" {
            _freeSize = pointer.swappedAsInt()
        }
    }
    
    override private func _setValue<Type>(key: String, value: Type?) {
        super._setValue(key, value: value)
        _keyOrder.append(key)
    }
    
    private func _writeSampleTableOffsets(sizeOffset:UInt32) {
        let stcoTag = "stco".dataUsingEncoding(NSUTF8StringEncoding)!
        do {
            let stcoTagIndex = try TagFinder._findTag(inFile: file, tag: stcoTag) - 4
            file.seekToFileOffset(UInt64(stcoTagIndex))
            
            let stcoSize = UnsafePointer<UInt32>(file.readDataOfLength(4).bytes)[0].byteSwapped
            file.seekToFileOffset(UInt64(stcoTagIndex))
            let stcoData = file.readDataOfLength( Int(stcoSize) ).mutableCopy() as! NSMutableData
            
            var pointer = stcoData.bytes.uint32Pointer()
            let stcoOffsetCount = (pointer + 3)[0].byteSwapped
            
            pointer += 4
            var i = 0
            while i < Int(stcoOffsetCount) {
                let offset = pointer[i].byteSwapped
                var newOffset = (offset + sizeOffset).byteSwapped
                let chunkDataOffset = 16+(i*4)
                stcoData.replaceBytesInRange(NSMakeRange(chunkDataOffset, 4), withBytes: &newOffset)
                i += 1
            }
            file.seekToFileOffset(UInt64(stcoTagIndex))
            file.writeData(stcoData)
            
            
        } catch let err {
            print("error finding stco tag offset \(err)")
        }
    }
    
    public override func saveToFile() {
        
        let metadata = NSMutableData()
        let nullBytes:[UInt8] = [0x00, 0x00, 0x00, 0x00]
        
        metadata.appendBytes(nullBytes, length: 4)
        metadata.appendBytes("meta", length: 4)
        metadata.appendBytes(nullBytes, length: 4)
        
        if let headerData = _headerData {
            metadata.appendData(headerData)
        }
        
        
        let ilistData = NSMutableData()
        ilistData.appendBytes(nullBytes, length: 4)
        ilistData.appendBytes("ilst", length: 4)
        for k in _keyOrder {
            if let itemData = items[k]!.encode() {
                ilistData.appendData(itemData)
            }
        }
        var ilistSize = UInt32(ilistData.length).byteSwapped
        ilistData.replaceBytesInRange(NSMakeRange(0, 4), withBytes: &ilistSize)
        metadata.appendData(ilistData)

        

        var needsToMoveData = false
        var newFreeSize:Int = 0
        if metadata.length <= (originalMetaSize - 8) { //Decrease or keep same free size but don't move
            newFreeSize = originalMetaSize - metadata.length
        } else if metadata.length > (originalMetaSize - 8) {
            newFreeSize = 1024
            needsToMoveData = true
        }
        

        var freeSizeSwap = UInt32(newFreeSize).byteSwapped


        let freeData = NSMutableData()
        freeData.appendBytes(&freeSizeSwap, length: 4)
        freeData.appendBytes("free", length: 4)
        freeData.appendBytes(UnsafeMutablePointer<UInt8>.alloc(newFreeSize-8), length: (newFreeSize-8))
        metadata.appendData(freeData)
        var newMetaSize = UInt32(metadata.length).byteSwapped
        metadata.replaceBytesInRange(NSMakeRange(0, 4), withBytes: &newMetaSize)
        
        if !needsToMoveData {
            file.seekToFileOffset(UInt64(_tagIndex - 4))
            file.writeData(metadata)
            
        } else {
            let metaOffset = (_tagIndex - 4)
            let metaDiffSize = metadata.length - originalMetaSize
            let afterDataOffset = UInt64(_tagIndex - 4 + originalMetaSize)
            
            file.seekToFileOffset(afterDataOffset)
            let dataAfterMeta = file.readDataToEndOfFile()
            
            var moovSize = (_moovTagSize + UInt32(metaDiffSize)).byteSwapped
            var udtaSize = (_udtaTagSize + UInt32(metaDiffSize)).byteSwapped
            
            file.seekToFileOffset(UInt64(_moovTagIndex))
            file.writeData(NSData(bytes: &moovSize, length: 4))
            
            file.seekToFileOffset(UInt64(_udtaTagIndex))
            file.writeData(NSData(bytes: &udtaSize, length: 4))
            
            _writeSampleTableOffsets(UInt32(metaDiffSize))

            file.seekToFileOffset(UInt64(metaOffset))
            file.writeData(metadata)
            file.writeData(dataAfterMeta)
        }
 
    }
}


public class Mp3Metadata : Metadata {
    
    private var _freeSize:Int = 0
    private var _majorVersion:UInt8 = 0
    private var _minorVersion:UInt8 = 0
    private var _flags:UInt8 = 0
    private var _audioDataOffset:UInt64 = 0
    private var _pictures:[ID3AlbumArtItem] = []
    private var _extras:[String:ID3TextExtraItem] = [:]
    
    override public var track: MetaSequence? {
        set (value) {
            if let v = value {
                let index:UInt16 = v.index != nil ? UInt16(v.index!) : 0
                let total:UInt16 = v.total != nil ? UInt16(v.total!) : 0

                if let item = items[type.track] as? ID3SequenceItem {
                    item.index = index
                    item.total = total
                    
                } else {
                    let item = ID3SequenceItem(tag: type.track)
                    item.index = index
                    item.total = total
                    items[type.track] = item
                }
            }
            
        }
        get {
            if let item = items[type.track] as? ID3SequenceItem {
                let index:Int? = item.index != nil ? Int(item.index!) : nil
                let total:Int? = item.total != nil ? Int(item.total!) : nil
                return MetaSequence(index: index, total: total)
            }
            return nil
        }
    }
    
    override public var disc: MetaSequence? {
        set (value) {
            if let v = value {
                let index:UInt16 = v.index != nil ? UInt16(v.index!) : 0
                let total:UInt16 = v.total != nil ? UInt16(v.total!) : 0
                
                if let item = items[type.disc] as? ID3SequenceItem {
                    item.index = index
                    item.total = total
                    
                } else {
                    let item = ID3SequenceItem(tag: type.disc)
                    item.index = index
                    item.total = total
                    items[type.track] = item
                }
            }
            
        }
        get {
            if let item = items[type.disc] as? ID3SequenceItem {
                let index:Int? = item.index != nil ? Int(item.index!) : nil
                let total:Int? = item.total != nil ? Int(item.total!) : nil
                return MetaSequence(index: index, total: total)
            }
            return nil
        }
    }
    
    #if os(OSX)
    override public var albumArt: NSImage? {
        set (value) {
            let coverPicture = _pictures.filter { $0.pictureType == .coverFront }
            let coverItem:ID3AlbumArtItem
            if coverPicture.count > 0 {
                coverItem = coverPicture[0]
                coverItem.mimeType = "image/jpeg"
                coverItem.value = value?.JPGData
            } else if _pictures.count > 0 {
                coverItem = _pictures[0]
                coverItem.mimeType = "image/jpeg"
                coverItem.value = value?.JPGData
            } else {
                coverItem = ID3AlbumArtItem(tag: type.albumArt)
                coverItem.pictureType = .coverFront
                coverItem.mimeType = "image/jpeg"
                coverItem.value = value?.JPGData
                _pictures.append(coverItem)
            }
            
        }
        get {
            let coverPicture = _pictures.filter { $0.pictureType == .coverFront }
            var coverItem:ID3AlbumArtItem?
            if coverPicture.count > 0 {
                coverItem = coverPicture[0]
            } else if _pictures.count > 0 {
                coverItem = _pictures[0]
            }
            return coverItem?.imageData
        }
    }
    #endif
    
    
    override public var energyLevel:String?  {
        set (value) {
            super.energyLevel = value
            if let energyLevelExtra = _extras["EnergyLevel"] {
                energyLevelExtra.value = value?.dataUsingEncoding(NSUTF8StringEncoding)
            }
        }
        get {
            return items[type.key]?.stringValue
        }
    }
    
    override private func _getMetadataFromFile(tagIndex:Int) -> NSData {
        let metadataOffset = UInt64(tagIndex)
        file.seekToFileOffset(metadataOffset)
        
        let headerPointer = file.readDataOfLength(10).bytes.uint8Pointer()
        let pointer = headerPointer + 6
        
        let size = pointer.syncSafe24BitInt()
        file.seekToFileOffset(metadataOffset)
        return file.readDataOfLength(size)
    }
    
    
    override private func _parse(data:NSData) throws {
        var ptr = data.bytes.uint8Pointer()
        _majorVersion = ptr[3]
        _minorVersion = ptr[4]
        _flags = ptr[5]
        let hasExtendedHeader = Bool(Int((_flags & 0b0100000) >> 6))
        let sizePointer = (ptr + 6)
        originalMetaSize = sizePointer.syncSafe24BitInt()
        
        var skipBytes = 10
        if hasExtendedHeader {
            let extSize = (ptr + 10).syncSafe24BitInt()
            skipBytes += extSize
        }
        let _ = Bool(Int((_flags & 0b0010000) >> 5)) //hasFooter
        
        ptr += skipBytes
        
        var offset = skipBytes
        while offset < data.length {
            if ptr[0] == 0 && ptr[1] == 0 && ptr[2] == 0 {
                break
            }
            let frameTag = String(data: NSData(bytes: ptr, length: 4), encoding: NSUTF8StringEncoding)!
            let frameSize = _majorVersion == 4 ? (ptr + 4).syncSafe24BitInt() : Int((ptr + 4).uint32Swapped())
            let firstCharacter = frameTag[frameTag.startIndex]
            
            let data = NSData(bytes: ptr, length: (frameSize + 10))
            
            if frameTag == type.comment {
                let item = ID3CommentItem(tag: frameTag, data: data )
                items[item.tag] = item
            } else if frameTag == type.albumArt {
                let item = ID3AlbumArtItem(tag: frameTag, data: data)
                _pictures.append(item)
            } else if frameTag == type.track || frameTag == type.disc {
                let item = ID3SequenceItem(tag: frameTag, data: data )
                items[item.tag] = item
            } else if frameTag == "TXXX" {
                let item = ID3TextExtraItem(tag: frameTag, data: data)
                _extras[item.key] = item
            } else if firstCharacter == "T" {
                let item = ID3MetadataItem(tag: frameTag, data: data)
                items[item.tag] = item
            } else {
                let item = ID3OtherItem(tag: frameTag, data: data)
                
                items[item.tag] = item
            }

            offset += frameSize + 10
            ptr += frameSize + 10   
        }
        
        _freeSize = data.length - (offset-10)
        _audioDataOffset = UInt64(offset + _freeSize)
    }
    
    
    public override func saveToFile() {
        let metadata = NSMutableData()
        
        let listData = NSMutableData()
        for (_, item) in items {
            if let itemData = item.encode() {
                listData.appendData(itemData)
            }
        }
        
        for (_, extra) in _extras {
            if let itemData = extra.encode() {
                listData.appendData(itemData)
            }
        }
        
        for pic in _pictures {
            if let itemData = pic.encode() {
                listData.appendData(itemData)
            }
        }
        
        var needsToMoveData = false
        var newFreeSize:Int = 0
        if listData.length <= (originalMetaSize - _freeSize) { //Decrease or keep same free size but don't move
            newFreeSize = originalMetaSize - listData.length
        } else if listData.length > originalMetaSize {
            newFreeSize = 10240
            needsToMoveData = true
        }
        
        let newEstimatedSize = listData.length + newFreeSize
        var syncNewSize = newEstimatedSize.syncSafe24Bit()
        
        metadata.appendBytes("ID3", length: 3)
        metadata.appendBytes(&_majorVersion, length: 1)
        metadata.appendBytes(&_minorVersion, length: 1)
        metadata.appendBytes(&_flags, length: 1)
        metadata.appendBytes(&syncNewSize, length: 4)
        metadata.appendData(listData)
        metadata.appendBytes(UnsafeMutablePointer<UInt8>.alloc(newFreeSize), length: newFreeSize)
        
        if !needsToMoveData {
            file.seekToFileOffset(UInt64(_tagIndex))
            file.writeData(metadata)
        } else {
            file.seekToFileOffset(_audioDataOffset)
            let dataAfterMeta = file.readDataToEndOfFile()
            
            file.seekToFileOffset(UInt64(_tagIndex))
            file.writeData(metadata)
            file.writeData(dataAfterMeta)
        }
    }
}